코드이그나이터 mvc자동 생성기
------------------------

### 설명 (Ver 1.0) ###
*  Codeigniter 사용시 일일이 파일생성의 불편함을 간단한 명령어로 손쉽게 파일을 생성 

*  리눅스,osx에서 만사용가능(윈도우에선 테스트 해보지 않음)

*  파일 테스트시 일반 폴더에서 복사후 ci폴더체크 부분(28,29)을 주석처리하고 실행하면 폴생성및 파일 생성가능


### 사용방법 ###
1.파일을 다운받은후 codeigniter의 index.php가 있는 동일한 위치에 복사 (권한이 필요한경우 적당한 권한 부여)

> 1-1. 명령어: ./mvcbuilder.php 

2.controller 생성

>  2-1. 명령어: Enter a command: g controller controller_name method1 method2

> >  생성위치 application/controllers/controller_name.php

>  2-2. method1은 컨트롤러안에 들어가는 메소드이름이며 여러개를 가질수다 

> >  메소드명으로 views/controller_name/method1.php형식으로 뷰가생성  

> 2-3. 컨트롤러 생성시 동시에 css와 js 컨트롤러명으로 생성

> > assets폴더가 존재 하지 않으면 자동 생성

> >  assets/js/controller_name.js

> >  assets/css/controller_name.css 

3.controller 삭제

> 3-1. 삭제명령어 실행하면 css,js controller, view 파일이 자동 삭제

> >  명령어: d controller controller_name

4.model 생성
> 4-1.  모델 생성시 models 폴더에 생성
> >  명령어: g model model_name

5.model삭제
> 5-1.  모델 삭제 명어를 실행시 자동 모델파일 삭제
> >   명령어: d model model_name 