#! /usr/bin/php
<?php
/**
 * Auto MVC builder
 * @version  1.0
 * @author   Seoung jin kim
 * @license  
 */


class Mvcbuilder
{
	private $current_path; #현재 디렉토리
	private $chk_folder;   #ci폴더인지 간단히 조
	private $cli_msg_colors; #커맨드라인 메시지 컬
	private $create_path;

	function __construct()
	{
		$this->current_path = getcwd();
		$this->chk_folder	= self::chk_folder("application");
		$this->cli_msg_colors = new Colors();
		$this->create_path = self::create_path();
	}
/*
-----------------------------------------------------------------------------------------
| 메인 실행 메소드 
-----------------------------------------------------------------------------------------
*/
	public function main()
	{

		#ci 폴더 체크
		if(!$this->chk_folder)
		{
			self::rt_msg("error", "CI의 application 폴더가 존재하지 않습니다.");
			exit;
		} 

		#커맨드라인 
		self::_cli();

	}


/*
-----------------------------------------------------------------------------------------
| 메인 실행 메소드 
-----------------------------------------------------------------------------------------
*/


	#command line
	private function _cli()
	{
		echo $this->cli_msg_colors->getColoredString("Enter a command: ","cyan"); 

		$handle = fopen ("php://stdin","r");
		$line = fgets($handle);
		$input = preg_split("/\s/", self::line_filter($line));

		if($input[0] == "exit" || $input[0] == "q")
		{
			exit;
		}
		else
		{
			#컨트롤러 수정삭제	
			if ($input[1] == 'controller') {
 			
				if ($input[0] == 'g') 
				{
					self::create_controller($input); 
				}
				elseif ($input[0] =='d') 
				{
					self::destroy_controller($input[2]); 
				}
				self::_cli();

			}
			elseif ($input[1] == 'model') 
			{
				
				if ($input[0] == 'g') 
				{
					self::create_model($input);
				}
				elseif ($input[0] =='d') 
				{
					self::destroy_model($input[2]);	
				}
				self::_cli();

	 		} 
	 		else 
	 		{	 
				echo $this->cli_msg_colors->getColoredString("명령어가 잘못되었습니다.\r\n","red"); 
				self::_cli();
	 		}
		}


	}
/*
| ---------------------------------------------------------------------
| 필수 메소드
| 컨트롤러를 생성하고 컨트롤러명과 같은 view, css와 js파일을 정해져있는 폴더에 생성한다
| 명령어 : g controller controller_name index 
| ---------------------------------------------------------------------
 */
 	# crate controller javascript css
	private function create_controller($input){

		$create_cntrl_file = $this->create_path['create_cntrl_file'].$input[2].'.php';
		$create_css_file   = $this->create_path['create_css_file'].$input[2].'.css';
		$create_js_file    = $this->create_path['create_js_file'].$input[2].'.js';


		#컨트롤러 폴더 생
		if (is_file($create_cntrl_file)) {
			self::rt_msg("error", $input[2]."는  잘못된 컨트롤러 이름입니다.\r\n");
			exit;
		} else {
			if (!is_dir($this->create_path['create_cntrl_file'])) {
				mkdir($this->create_path['create_cntrl_file'], 0755, true);
			}
		}

		#check view directory
		if (!is_dir($this->create_path['create_views'].$input[2])) {
			mkdir($this->create_path['create_views'].$input[2], 0755, true);
		}

		#create controller file
		try {
			$f = fopen($create_cntrl_file, 'w+');
			fputs($f, "<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');"."\r\n");
			fputs($f,""."\r\n");
			fputs($f,"class ".ucwords($input[2])." extends CI_Controller {"."\r\n"); #ucwords => 첫글자 대문
			fputs($f,""."\r\n");

			if (count($input) > 3) {
				for ($i=3; $i < count($input); $i++) { 
					fputs($f,"	public function ".$input[$i]."() { "."\r\n");
					fputs($f,""."\r\n");
					fputs($f,"	}"."\r\n");
					fputs($f,""."\r\n");

					#views file
					$create_view_file = $this->create_path['create_views'].$input[2].'/'.$input[$i].'.php';

					$v = fopen($create_view_file, 'w+');
					fputs($v,"<h1>".$input[2]."##".$input[$i]."</h1>"."\r\n");
					fputs($v,""."\r\n");
					fclose($v);
					self::rt_msg("create", $create_view_file);
				}
			}

			#space
			$i = 0;
			while (++$i < 4) {
				fputs($f,""."\r\n");
			}

			fputs($f,"}\r\n"); 
			fputs($f,"/* End of file ".$input[2].".php */"."\r\n");
			fclose($f);
			self::rt_msg("create", $create_cntrl_file);

		} catch (Exception $e) {
			self::rt_msg("error", $e);
		}
		


		#generate js file
		#create folder
		if (!is_dir($this->create_path['create_js_file'])) {
			mkdir($this->create_path['create_js_file'] , 0755, true);
		}

		#generate js
		try{
			if (!is_dir($this->create_path['create_js_file'])) {
				throw new Exception("Error Processing Request", 1);
			}else{
				$j = fopen($create_js_file, 'w+');
				fputs($j, ""."\r\n");
				fclose($j);
				self::rt_msg("create", $create_js_file);
			}

		}catch(Exception $e){
			// echo $e;
			self::rt_msg("error", "javascript 파일을 생성할수 없습니다."); 
		}
		

		#generate css file
		#create folder
		if (!is_dir($this->create_path['create_css_file'])) {
			mkdir($this->create_path['create_css_file'] , 0755, true);
		}

		#generate css
		try{
			if(!is_dir($this->create_path['create_css_file'])){
				throw new Exception("Error Processing Request", 1);
			}else{
				$c = fopen($create_css_file, 'w+');
				fputs($c, ""."\r\n");
				fclose($c);
				self::rt_msg("create", $create_css_file);
			}

		}catch(Exception $e){
			//echo $e;
			self::rt_msg("error", "css 파일을 생성할수 없습니다."); 
		}
 

	}

/*
----------------------------------------
|  생성된 controller,js css view 파일 삭제 
 ---------------------------------------
*/

	#destroy controller javascript css
	private function destroy_controller($input){
		$create_cntrl_file = $this->create_path['create_cntrl_file'].$input.'.php';
		$create_css_file   = $this->create_path['create_css_file'].$input.'.css';
		$create_js_file    = $this->create_path['create_js_file'].$input.'.js';
		$file_url_arr      = array($create_cntrl_file, $create_css_file, $create_js_file);

		for ($i=0; $i < count($file_url_arr); $i++) { 
			 if (is_file($file_url_arr[$i])) {
			 	unlink($file_url_arr[$i]);

			 	self::rt_msg("remove", $file_url_arr[$i]);
			 }else{
			 	self::rt_msg("remove", $file_url_arr[$i]."는 존재 하지 않는 파일입니다.");
			 }
		}

		#destroy view directory 
		if (is_dir($this->create_path['create_views'].$input)) {
			exec("rm -rf ".$this->create_path['create_views'].$input); #폴더안에 있는 모든 파일 삭제
			self::rt_msg("remove", $this->create_path['create_views'].$input);
		}
	}

/*
---------------------
| 모델 생성
---------------------
 */

	#create model
	private function create_model($input){
		#file_path
		$create_model = $this->create_path['create_model'].$input[2].'.php';

		if (is_file($create_model)) {
			self::rt_msg("error", $input[2].".php는  잘못된 모델 이름입니다.\r\n");
			exit;
		}
		//  elseif(!is_file($create_model)) {
		// 	mkdir($this->create_path['create_model'], 0755, true);
		// }
 

		$m = fopen($create_model, 'w+');
		fputs($m,"<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); "."\r\n");
		fputs($m, "\r\n");
		fputs($m, "class ".ucwords($input[2])." extends CI_Model {"."\r\n");

		#space
 		$i = 0;
		while (++$i < 4) {
			fputs($m,""."\r\n");
		}
		fputs($m, "}"."\r\n");
		fputs($m, "\r\n");
		fputs($m, "/* End of file ".$input[2].".php */"."\r\n");
		fputs($m, "\r\n");
		fclose($m);

		self::rt_msg("create", $create_model);

	}

/*
---------------------
| 모델 삭제
---------------------
 */

	#destroy model
	private function destroy_model($input){
		#file_path
		$destroy_model = $this->create_path['create_model'].$input.'.php';

		if (is_file($destroy_model)) {
			 	unlink($destroy_model);
			 	self::rt_msg("remove", $destroy_model);
		}else{
			self::rt_msg("create", $input.".php 는 존재 하지 않는 파일입니다.\r\n");			
		}

	}






/*
------------
| 라이브러리 
------------
*/

	#유효한 폴더 체크
	private function chk_folder($folder)
	{
		return is_dir($this->current_path."/".$folder);
	}

	#applicaton path
	private function create_path()
	{

		$this->current_path = str_replace(basename(__FILE__), '', realpath(__FILE__));
		$app_path = array(
			              'create_cntrl_file' => $this->current_path.'application/controllers/', 
			              'create_css_file' => $this->current_path.'assets/css/', 
			              'create_js_file' => $this->current_path.'assets/js/', 
			              'create_model' => $this->current_path.'application/models/', 
			              'create_views' => $this->current_path.'application/views/'
			              );
		return $app_path;
	}

	#input value filter
	private function line_filter($line)
	{
		$pattern1 = "/[~!\#$^&*\=+|:;?\"<,.>']/"; #특수문자
		$pattern2 = '/\//' ;
		$pattern3 = "/([\s]{1,})/" ;#1개이상의 공백
		$pattern4 = "/\r\n|\r|\n/"; #엔터
		$this->line = $line;

		if ($line !='') {
			$this->line = preg_replace($pattern1, "", trim($this->line));
			$this->line = preg_replace($pattern2, "", $this->line);
			$this->line = preg_replace($pattern3, " ", $this->line);
			$this->line = preg_replace($pattern4,"", $this->line);
		}

		return $this->line;
	}


	#result message
	private function rt_msg($val1, $f_path)
	{
		$color = ($val1 == 'create') ? 'light_green' : 'light_red' ;
		$msg1 = $this->cli_msg_colors->getColoredString($val1,$color);
		$msg2 = $this->cli_msg_colors->getColoredString($f_path."\r\n","magenta");
		echo $msg1."  ".$msg2;
	}



}

/*
---------------------
| console color
---------------------
 */

class Colors {
		private $foreground_colors = array();
		private $background_colors = array();
 
		public function __construct() {
			// Set up shell colors
			$this->foreground_colors['black'] = '0;30';
			$this->foreground_colors['dark_gray'] = '1;30';
			$this->foreground_colors['blue'] = '0;34';
			$this->foreground_colors['light_blue'] = '1;34';
			$this->foreground_colors['green'] = '0;32';
			$this->foreground_colors['light_green'] = '1;32';
			$this->foreground_colors['cyan'] = '0;36';
			$this->foreground_colors['light_cyan'] = '1;36';
			$this->foreground_colors['red'] = '0;31';
			$this->foreground_colors['light_red'] = '1;31';
			$this->foreground_colors['purple'] = '0;35';
			$this->foreground_colors['light_purple'] = '1;35';
			$this->foreground_colors['brown'] = '0;33';
			$this->foreground_colors['yellow'] = '1;33';
			$this->foreground_colors['light_gray'] = '0;37';
			$this->foreground_colors['white'] = '1;37';
 
			$this->background_colors['black'] = '40';
			$this->background_colors['red'] = '41';
			$this->background_colors['green'] = '42';
			$this->background_colors['yellow'] = '43';
			$this->background_colors['blue'] = '44';
			$this->background_colors['magenta'] = '45';
			$this->background_colors['cyan'] = '46';
			$this->background_colors['light_gray'] = '47';
		}

		// Returns colored string
		public function getColoredString($string, $foreground_color = null, $background_color = null) {
			$colored_string = "";
 
			// Check if given foreground color found
			if (isset($this->foreground_colors[$foreground_color])) {
				$colored_string .= "\033[" . $this->foreground_colors[$foreground_color] . "m";
			}
			// Check if given background color found
			if (isset($this->background_colors[$background_color])) {
				$colored_string .= "\033[" . $this->background_colors[$background_color] . "m";
			}
 
			// Add string and end coloring
			$colored_string .=  $string . "\033[0m";
 
			return $colored_string;
		}
 
		// Returns all foreground color names
		public function getForegroundColors() {
			return array_keys($this->foreground_colors);
		}
 
		// Returns all background color names
		public function getBackgroundColors() {
			return array_keys($this->background_colors);
		}
	}

/*
---------------------
| 실행
---------------------
 */
$abc = new Mvcbuilder();
$abc->main();
 
?>